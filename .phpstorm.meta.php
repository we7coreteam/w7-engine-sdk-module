<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2022 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace PHPSTORM_META{
    registerArgumentsSet(
        'accountType',
        \W7\Sdk\Module\Support\Account::TYPE_NONE,
        \W7\Sdk\Module\Support\Account::TYPE_WECHAT,
        \W7\Sdk\Module\Support\Account::TYPE_WECHAT_MINI,
        \W7\Sdk\Module\Support\Account::TYPE_APP,
        \W7\Sdk\Module\Support\Account::TYPE_ALI_MINI,
        \W7\Sdk\Module\Support\Account::TYPE_BAIDU_MINI,
        \W7\Sdk\Module\Support\Account::TYPE_TIK_TOK_MINI,
        \W7\Sdk\Module\Support\Account::TYPE_WECHAT_WORK,
        \W7\Sdk\Module\Support\Account::TYPE_K_WAI_MINI,
        \W7\Sdk\Module\Support\Account::TYPE_WECHAT_EC,
        \W7\Sdk\Module\Support\Account::TYPE_QQ_MINI,
    );

    expectedArguments(\W7\Sdk\Module\Api::__construct(), 2, argumentsSet('accountType'));
}
