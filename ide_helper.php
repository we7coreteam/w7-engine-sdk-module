<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2022 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\Module{
    use AccountListResult;
    use AllLinkResult;
    use LoginResult;

    class Api
    {
        /**
         * 获取关联的平台
         *
         * @return AccountListResult|array<int, array{
         *     name: string,
         *     type: int,
         *     app_id: string,
         *     access_type: ?numeric-string,
         *     account_type: ?numeric-string,
         *     logo_url: ?string
         * }>
         *
         * @noinspection PhpReturnDocTypeMismatchInspection
         */
        public function getAccountList()
        {
        }

        /**
         * 获取关联的平台
         *
         * @return AllLinkResult|array<int, array{
         *     app_id: string,
         *     url: int,
         *     account: array<int, array{
         *         name: string,
         *         type: int,
         *         app_id: string,
         *         access_type: ?numeric-string,
         *         account_type: ?numeric-string,
         *         logo_url: ?string
         *     }>
         * }>
         *
         * @noinspection PhpReturnDocTypeMismatchInspection
         */
        public function getAllLink()
        {
        }

        /**
         * 登录获取站点创始人Token
         *
         * @return LoginResult|array{
         *     token: string
         * }
         *
         * @noinspection PhpReturnDocTypeMismatchInspection
         */
        public function login()
        {
        }
    }
}

namespace W7\Sdk\Module\Support{
    use AccessTokenResult;
    use JsCode2SessionResult;
    use SnsOauthAccessTokenResult;

    class AccountRequest
    {
        /**
         * 获取平台的AccessToken
         *
         * @return AccessTokenResult|array{
         *     access_token: string,
         *     expires_in: int
         * }
         *
         * @noinspection PhpReturnDocTypeMismatchInspection
         */
        public function getAccessToken()
        {
        }

        /**
         * 微信小程序登录
         *
         * @param string $js_code
         *
         * @return JsCode2SessionResult|array{
         *     session_key: string,
         *     unionid: ?string,
         *     openid: string,
         *     expires_in: int
         * }
         *
         * @noinspection PhpReturnDocTypeMismatchInspection
         */
        public function jsCode2Session(string $js_code)
        {
        }

        /**
         * 微信公众号授权登录
         *
         * @param string $code
         *
         * @return SnsOauthAccessTokenResult|array{
         *     access_token: string,
         *     expires_in: int,
         *     refresh_token: string,
         *     openid: string,
         *     scope: string,
         *     is_snapshotuser: ?int,
         *     unionid: ?string
         * }
         *
         * @noinspection PhpReturnDocTypeMismatchInspection
         */
        public function snsOauthAccessToken(string $code)
        {
        }
    }
}
namespace{
    class AccountListResult
    {
        /**
         * @return array<int, array{
         *     name: string,
         *     type: int,
         *     app_id: string,
         *     access_type: ?numeric-string,
         *     account_type: ?numeric-string,
         *     logo_url: ?string
         * }>
         */
        public function toArray()
        {
            return [];
        }
    }

    class AllLinkResult
    {
        /**
         * @return array<int, array{
         *     app_id: string,
         *     url: int,
         *     account: array<int, array{
         *         name: string,
         *         type: int,
         *         app_id: string,
         *         access_type: ?numeric-string,
         *         account_type: ?numeric-string,
         *         logo_url: ?string
         *     }>
         * }>
         */
        public function toArray()
        {
            return [];
        }
    }

    /**
     * @property string $token 和软擎授权系统交互的token
     */
    class LoginResult
    {
        /**
         * @return array{
         *     token: string
         * }
         */
        public function toArray()
        {
            return [];
        }
    }

    /**
     * @property string $access_token AccessToken
     * @property int    $expires_in   有效期(秒)
     */
    class AccessTokenResult
    {
        /**
         * @return array{
         *     access_token: string,
         *     expires_in: int
         * }
         */
        public function toArray()
        {
            return [];
        }
    }

    /**
     * @property string      $session_key 会话密钥
     * @property null|string $openid      用户唯一标识
     * @property int         $expires_in  有效期(秒)
     * @property string      $unionid     用户在开放平台的唯一标识符
     */
    class JsCode2SessionResult
    {
        /**
         * @return array{
         *     session_key: string,
         *     unionid: ?string,
         *     openid: string,
         *     expires_in: int
         * }
         */
        public function toArray()
        {
            return [];
        }
    }

    /**
     * @property string      $access_token    接口调用凭证
     * @property int         $expires_in      access_token接口调用凭证超时时间，单位（秒）
     * @property string      $refresh_token   用户刷新access_token
     * @property string      $openid          授权用户唯一标识
     * @property string      $scope           用户授权的作用域，使用逗号（,）分隔
     * @property null|int    $is_snapshotuser 是否是快照用户
     * @property null|string $unionid         用户在开放平台的唯一标识符
     *
     * @noinspection SpellCheckingInspection
     */
    class SnsOauthAccessTokenResult
    {
        /**
         * @return array{
         *     access_token: string,
         *     expires_in: int,
         *     refresh_token: string,
         *     openid: string,
         *     scope: string,
         *     is_snapshotuser: ?int,
         *     unionid: ?string
         * }
         */
        public function toArray()
        {
            return [];
        }
    }
}
