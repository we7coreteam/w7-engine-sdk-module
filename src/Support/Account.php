<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2022 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Sdk\Module\Support;

class Account
{
    /** 平台类型 - 无 */
    public const TYPE_NONE = 0;

    /** 平台类型 - 公众号 */
    public const TYPE_WECHAT = 1;

    /** 平台类型 - 微信小程序 */
    public const TYPE_WECHAT_MINI = 2;

    /** 平台类型 - APP */
    public const TYPE_APP = 4;

    /** 平台类型 - 支付宝小程序 */
    public const TYPE_ALI_MINI = 5;

    /** 平台类型 - 百度小程序 */
    public const TYPE_BAIDU_MINI = 6;

    /** 平台类型 - 抖音小程序 */
    public const TYPE_TIK_TOK_MINI = 7;

    /** 平台类型 - 企业微信 */
    public const TYPE_WECHAT_WORK = 8;

    /** 平台类型 - 快手网页小程序 */
    public const TYPE_K_WAI_MINI = 9;

    /** 平台类型 - 微信视频号 */
    public const TYPE_WECHAT_EC = 10;

    /** 平台类型 - QQ小程序 */
    public const TYPE_QQ_MINI = 11;
}
